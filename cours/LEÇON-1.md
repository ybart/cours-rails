# Les langages HTML et CSS

Dans ce cours, nous allons créer notre site web qui est un répertoire de chiens mignons.

## Comment fonctionne un site web

Avant de commencer à apprendre comment faire un site Internet, nous allons nous intéresser au fonctionnement d'un site web.

Pour consulter un site web, vous utilisez un logiciel de votre ordinateur qu'on appelle 'navigateur web'.

Connaissez-vous des exemples de navigateur web ?

Les navigateurs web les plus répandus sont Google Chrome, Windows Internet Explorer, Mozilla Firefox et Apple Safari&nbsp;:

![Chrome Icon](images/chrome-icon.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Edge Icon](images/edge-icon.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Firefox Icon](images/firefox-icon.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Safari Icon](images/safari-icon.png)

Voici comment s'affiche une page web dans le navigateur web Safari&nbsp;:

![Page web](images/page-web.png)

Vous connaissez les sites web et vous les utilisez probablement tous les jours. Mais comment faire un site web ?

Vous savez probablement qu'un site web est composé de pages liées entre elles, mais de quoi a-t-on besoin pour créer une page web ?

Parlons des langages HTML et CSS dont vous avez sûrement entendu le nom. Ce sont des langages informatiques qui permettent de créer les sites Internet. En réalité une page web est un fichier informatique écrite dans le langage HTML.

Ce fichier est interprété par le navigateur web qui affiche le résultat obtenu à l'écran.

![Page Source](images/page-source.png)

## Notre première page HTML

Nous allons maintenant écrire notre première page HTML. Pour cela, nous allons utiliser un éditeur de texte.

Ouvrez le fichier exercice-1.html dans votre éditeur de texte qui nous servira de base pour la suite de cet exercice.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Exercice 1</title>
  </head>
  <body>
    Bonjour à tous !
  </body>
</html>
```

Affichez-le maintenant dans un navigateur&nbsp;:

![Ex1](images/ex1.png)

Nous allons maintenant analyser le code informatique de cette page.

### Les balises

Vous pouvez remarquer que le texte qui compose le document HTML contient des mots entre des chevrons `<` et `>`. On appelle ces instructions spéciales des balises. Elles fonctionnent par paire&nbsp;. Une balise ouvrante, par exemple `<html>` et une balise fermante comme `</html>`.

La balise fermante reprend toujours le nom de la balise ouvrante mais avec un `/` devant.

Elles servent à donner des indications sur le texte qu'elles entourent.

La première ligne indique simplement au navigateur qu'il s'agit bien d'une page HTML&nbsp;:

```html
<!DOCTYPE html>
```

C'est une balise spéciale qui n'a pas besoin d'être fermée (d'où le point d'exclamation).

La balise `html` sert à indiquer que le texte qui est entre `<html>` et `</html>` est le contenu du document HTML.

Les invformations contenues entre les balises `<head>` et `</head>` servent à donner des informations générales sur le document. Ces informations ne sont pas affichées sur la page elle-même mais elles donnent des informations importantes pour le navigateur.

Dans notre première page, il y a deux informations contenues dans la balise head&nbsp;:

 - La balise `meta` avec l'attrbut `charset` définit l'encodage de la page qui permet d'afficher le texte correctement. Il faut que l'encodage indiqué corresponde au codage de votre document (vous pouvez paramétrer cette information dans votre éditeur de texte). L'encodage UTF-8 permet d'afficher des textes dans toutes les langues du monde.
 - La balise `title` définit le titre de la page que le navigateur va afficher sur l'onglet, ou que les moteurs de recherche comme Google vont afficher dans leurs résultats.

Enfin les balises `<body>` et `</body>` indique que le texte délimité est le corps du document. Sur notre page, le texte est 'Bonjour à tous !'

Et voilà, nous avons écrit notre toute première page web.

Nous allons ensuite étudier les différents éléments du HTML.

## Les différentes balises du HTML

Pour organiser le contenu d'une page web, le HTML permet d'utiliser de nombreuses balises. Ces balises permettent de structurer le contenu de votre page. Ce sont des indications qui vont permettre au navigateur de donner un rôle à chaque partie de la page.

Prenons par exemple une page de resultats d'un moteur de recherche&nbsp;:

![Recherche Chiens Mignons](images/recherche-chiens-mignons.png)

Elle est divisée en plusieurs parties de haut en bas&nbsp;:

- La barre de recherche
- La barre de navigation
- Les informations sur les résultats
- Les résultats de recherche

Chaque partie a sa propre organisation. Par exemple, la barre de recerche contient un logo, le champ de recherche, le bouton des services, le bouton connexion.

Pour délimiter les différentes parties d'une page, le langage HTML permet d'utiliser différentes balises :
