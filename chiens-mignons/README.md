# Exemple de configuration Gulp

Composants requis&nbsp;:

- Compass
- NodeJS (npm)

Installation&nbsp;:

```
gem install compass
npm install
```

Utilisation&nbsp;:

```
gulp watch
```
