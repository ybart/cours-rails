var gulp = require('gulp'),
    compass = require('gulp-compass'),
    notify = require("gulp-notify"),
    uncss = require("gulp-uncss"),
    livereload = require("gulp-livereload"),
    path = require('path');

gulp.task('compass', function() {
  gulp.src('./assets/sass/**/*.scss')
    .pipe(compass({
      config_file: './config.rb',
      css: 'assets/stylesheets',
      sass: 'assets/sass',
      images: 'assets/images'
    }))
    .pipe(gulp.dest('assets/stylesheets'))
    .pipe(uncss({
      html: ['index.html']
    }))
    .pipe(gulp.dest('assets/stylesheets'));
});

gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('assets/sass/**/*.scss', ['compass']);
});
